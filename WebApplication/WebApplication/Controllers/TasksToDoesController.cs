﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PerfectChannel.WebApi.Models;
using WebApplication.Data;

namespace WebApplication.Controllers
{
    [EnableCors("AllowOrigin")]
    public class TasksToDoesController : Controller
    {
        private readonly WebApplicationContext _context;

        public TasksToDoesController(WebApplicationContext context)
        {
            _context = context;
        }

        // GET: TasksToDoes
        public async Task<IActionResult> Index()
        {
            return View(await _context.TasksToDo.ToListAsync());
        }

        // GET: TasksToDoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tasksToDo = await _context.TasksToDo
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tasksToDo == null)
            {
                return NotFound();
            }

            return View(tasksToDo);
        }

        // GET: TasksToDoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TasksToDoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Desc,Completed")] TasksToDo tasksToDo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tasksToDo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tasksToDo);
        }

        // GET: TasksToDoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tasksToDo = await _context.TasksToDo.FindAsync(id);
            if (tasksToDo == null)
            {
                return NotFound();
            }
            return View(tasksToDo);
        }

        // POST: TasksToDoes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Desc,Completed")] TasksToDo tasksToDo)
        {
            if (id != tasksToDo.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tasksToDo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TasksToDoExists(tasksToDo.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tasksToDo);
        }

        // GET: TasksToDoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tasksToDo = await _context.TasksToDo
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tasksToDo == null)
            {
                return NotFound();
            }

            return View(tasksToDo);
        }

        // POST: TasksToDoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tasksToDo = await _context.TasksToDo.FindAsync(id);
            _context.TasksToDo.Remove(tasksToDo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TasksToDoExists(int id)
        {
            return _context.TasksToDo.Any(e => e.Id == id);
        }
    }
}
