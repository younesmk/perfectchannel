﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PerfectChannel.WebApi.Data;
using PerfectChannel.WebApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PerfectChannel.WebApi.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowOrigin")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        // TODO: to be completed

        private readonly PerfectChannelWebApiContext _context;

        public TaskController(PerfectChannelWebApiContext context)
        {
            _context = context;
        }

        // GET: TasksToDoes
        // GET: api/TasksToDoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TasksToDo>>> GetTasksToDo()
        {
            return await _context.TasksToDo.ToListAsync();
        }

        // GET: api/TasksToDoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TasksToDo>> GetTasksToDo(int id)
        {
            var tasksToDo = await _context.TasksToDo.FindAsync(id);

            if (tasksToDo == null)
            {
                return NotFound();
            }

            return tasksToDo;
        }

        // PUT: api/TasksToDoes/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTasksToDo(int id, TasksToDo tasksToDo)
        {
            if (id != tasksToDo.Id)
            {
                return BadRequest();
            }

            _context.Entry(tasksToDo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TasksToDoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TasksToDoes
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TasksToDo>> PostTasksToDo(TasksToDo tasksToDo)
        {
            _context.TasksToDo.Add(tasksToDo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTasksToDo", new { id = tasksToDo.Id }, tasksToDo);
        }

        // DELETE: api/TasksToDoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TasksToDo>> DeleteTasksToDo(int id)
        {
            var tasksToDo = await _context.TasksToDo.FindAsync(id);
            if (tasksToDo == null)
            {
                return NotFound();
            }

            _context.TasksToDo.Remove(tasksToDo);
            await _context.SaveChangesAsync();

            return tasksToDo;
        }

        private bool TasksToDoExists(int id)
        {
            return _context.TasksToDo.Any(e => e.Id == id);
        }
    }
}