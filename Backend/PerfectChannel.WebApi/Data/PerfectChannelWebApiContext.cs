﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PerfectChannel.WebApi.Models;

namespace PerfectChannel.WebApi.Data
{
    public class PerfectChannelWebApiContext : DbContext
    {
        public PerfectChannelWebApiContext (DbContextOptions<PerfectChannelWebApiContext> options)
            : base(options)
        {
        }

        public DbSet<PerfectChannel.WebApi.Models.TasksToDo> TasksToDo { get; set; }
    }
}
